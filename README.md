# About

A sample Spring app for restful demo.

## Build

```shell
gradlew clean build
```

## Usage

```shell
java -jar ./build/libs/spring-restful-sample-0.1.0.jar
```

After the application was started, hit the API with:

```txt
http://localhost:8080/greetings?name=John
```

will get response:

```json
{
    "id": 1,
    "content": "What's up, John!"
}
```

### Healthcheck Endpoint

Actuator was installed for health checks.

Checkout links available for the endpoints:

```txt
http://localhost:8080/actuator
```
