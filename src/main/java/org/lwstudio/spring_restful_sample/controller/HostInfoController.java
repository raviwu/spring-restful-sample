package org.lwstudio.spring_restful_sample.controller;

import org.lwstudio.spring_restful_sample.model.HostInfo;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HostInfoController {

    @RequestMapping("/hostinfo")
    public HostInfo greeting() {
        return new HostInfo();
    }

}
