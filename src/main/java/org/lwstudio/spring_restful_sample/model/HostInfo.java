package org.lwstudio.spring_restful_sample.model;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class HostInfo {
    private String hostname;
    private String hostAddress;

    public HostInfo() {
        try {
            InetAddress address = InetAddress.getLocalHost();
            this.hostname = address.getHostName();
            this.hostAddress = address.getHostAddress();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }

    public String getHostname() {
        return hostname;
    }

    public String getHostAddress() {
        return hostAddress;
    }
}
