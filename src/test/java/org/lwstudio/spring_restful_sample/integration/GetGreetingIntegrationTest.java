package org.lwstudio.spring_restful_sample.integration;

import org.lwstudio.spring_restful_sample.model.Greeting;

import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class GetGreetingIntegrationTest {
    private static String greeting = "What's up, ";

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void canGetDefaultGreeting() {
        ResponseEntity<Greeting> greetingResponse = restTemplate.getForEntity("/greeting", Greeting.class);

        assertEquals(greetingResponse.getStatusCode(), HttpStatus.OK);
        assertEquals(greetingResponse.getBody().getContent(), greeting + "world!");
    }

    @Test
    public void canGetCustomGreeting() {
        String customName = "Ravi";

        ResponseEntity<Greeting> greetingResponse = restTemplate.getForEntity("/greeting?name=" + customName,
                Greeting.class);

        assertEquals(greetingResponse.getStatusCode(), HttpStatus.OK);
        assertEquals(greetingResponse.getBody().getContent(), greeting + customName + "!");
    }

}
